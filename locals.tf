# ws@2023 locals.tf

##################################################################
# LOCALS
##################################################################

locals {

  # create
  enabled = var.module_enabled ? true : false
  create  = var.module_enabled ? true : false
    
    tags = merge(
        var.module_tags,
        {
        Terraform = true
        }
    )
}

