# ws@2023

# #################################################################
# Create IAM Group: ECRFullAccessGrop
# #################################################################

module "iam_group_ecr_full_access" {

  source  = "git::https://gitlab.com/cloud7392819/aws1872017/tf-modules/security/iam/tf-aws-iam-group.git?ref=tags/0.0.1"

  module_enabled = true

  iam_group_name        = "${local.cut_name}-ECRFullAccessGrop"
  iam_group_path        = "/"
  iam_group_policy_name = "${local.cut_name}-ECRFullAccessPolicy"

  iam_group_policy_document = <<-EOF
  {
    "Version": "2012-10-17",
    "Statement": [
      {
        "Effect": "Allow",
        "Action": [
          "ecr:GetAuthorizationToken",
          "ecr:BatchCheckLayerAvailability",
          "ecr:GetDownloadUrlForLayer",
          "ecr:GetRepositoryPolicy",
          "ecr:DescribeRepositories",
          "ecr:ListImages",
          "ecr:DescribeImages",
          "ecr:BatchGetImage",
          "ecr:InitiateLayerUpload",
          "ecr:UploadLayerPart",
          "ecr:CompleteLayerUpload",
          "ecr:PutImage"
        ],
        "Resource": "*"
      }
    ]
  }
  EOF

  iam_group_policies_list = [
    #"",
  ]

  iam_group_users = [
    #"",
  ]
}