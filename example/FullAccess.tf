# ws@2023

# #################################################################
# Create IAM GROUP: FullAccessPolicy"
# #################################################################

module "iam_group_full_access" {
 
  source  = "git::https://gitlab.com/cloud7392819/aws1872017/tf-modules/security/iam/tf-aws-iam-group.git?ref=tags/0.0.1"

  module_enabled = true
  
  iam_group_name = "${local.cut_name}-FullAccessGroup"
  iam_group_path = "/"
  iam_group_policy_name = "${local.cut_name}-FullAccessPolicy"

  iam_group_policy_document = <<-EOF
  {
  "Version": "2012-10-17",
    "Statement": [
      {
        "Effect": "Allow",
        "Action": "*",
        "Resource": "*"
      }
    ]
  }
  EOF

  iam_group_policies_list = [
    #module.iam_policy_force_mfa.iam_policy_arn,
    #module.iam_policy_change_password.iam_policy_arn
  ]

  iam_group_users = [
    #"",
  ]
}