# #################################################################
# Create IAM Group: "ZabbixAccessGroup"
# #################################################################

module "zabbix" {

  source = "git::https://gitlab.com/cloud7392819/aws1872017/tf-modules/security/iam/tf-aws-iam-group.git?ref=tags/0.0.1"
  #source = "./modules/.."

  create = false

  iam_group_name        = "${local.cut_name}-ZabbixAccessGroup"
  iam_group_path        = "/"
  iam_group_policy_name = "${local.cut_name}-ZabbixAccessPolicy"

  iam_group_policy_document = <<-EOF
  {
    "Version": "2012-10-17",
    "Statement": [
      {
        "Effect": "Allow",
        "Action": [
          "ce:GetDimensionValues",
          "ce:GetCostAndUsage",
          "cloudwatch:Describe*",
          "cloudwatch:Get*",
          "cloudwatch:List*",
          "rds:Describe*",
          "ec2:Describe*",
          "ec2:AssociateIamInstanceProfile",
          "ec2:ReplaceIamInstanceProfileAssociation",
          "ecs:Describe*",
          "ecs:ListS*",
          "s3:List*",
          "s3:GetBucketLocation",
          "elasticloadbalancing:Describe*"
        ],
        "Resource": [
          "*"
        ]
      }
    ]
  }
  EOF

  iam_group_policies_list = [
    #"",
  ]

  iam_group_users = [
    #"zabbix",
  ]
}
