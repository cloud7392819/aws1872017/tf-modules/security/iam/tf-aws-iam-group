![Terraform](https://lgallardo.com/images/terraform.jpg)

# tf-aws-iam-group
This module manage AWS IAM Group [terraform-module-aws-iam-group].

===

## Manages an IAM groups.

## MODULE

```hcl-terraform
module "iam_group_<name>" {

  source = "git::https://gitlab.com/cloud7392819/aws1872017/tf-modules/security/iam/tf-aws-iam-group.git?ref=tags/0.0.1"

  module_enabled = false
  
  iam_group_name = "${var.cut_name}-<name>"
  iam_group_path = "/"
  iam_group_policy_name = "${var.cut_name}-<name>"

  iam_group_policy_document = <<-EOF
  {
  "Version": "2012-10-17",
    "Statement": [
      {
        "Effect": "Allow",
        "Action": "*",
        "Resource": "*"
      }
    ]
  }
  EOF

  iam_group_policies_list = [
    #"policy-arn"
  ]

  iam_group_users = [
    #"vasya.pupkin"
  ]
}
```

### IAM Group for FullAccessGroup

```hcl-terraform

# Create IAM GROUP: "${var.cut_name}-FullAccessPolicy"

module "iam_group_full_access" {

  source = "git::https://gitlab.com/cloud7392819/aws1872017/tf-modules/security/iam/tf-aws-iam-group.git?ref=tags/0.0.1"

  module_enabled = true | false
  
  iam_group_name = "${var.cut_name}-FullAccessGroup"
  iam_group_path = "/"
  iam_group_policy_name = "${var.cut_name}-FullAccessPolicy"

  iam_group_policy_document = <<-EOF
  {
  "Version": "2012-10-17",
    "Statement": [
      {
        "Effect": "Allow",
        "Action": "*",
        "Resource": "*"
      }
    ]
  }
  EOF

  iam_group_policies_list = [
    #"test3-policy-arn1"
  ]

  iam_group_users = [
  #"vasya.pupkin"
  ]
}
```



```hcl
# Create IAM Group: ChangePasswordGroup
module "iam_group_change_password" {
  
  source = "git::<url>?ref=1.0.0"

  module_enabled = true

  iam_group_name        = "${var.cut_name}-ChangePasswordGroup"   
  iam_group_path        = "/"
  iam_group_policy_name = "${var.cut_name}-ChangePasswordPolicy"

  iam_group_policy_document = <<-EOF
  {
  "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "iam:ChangePassword"
            ],
            "Resource": [
                "arn:aws:iam::*:user/$${aws:username}"
            ]
        },
        {
            "Effect": "Allow",
            "Action": [
                "iam:GetAccountPasswordPolicy"
            ],
            "Resource": "*"
        }
    ]
  }
  EOF

  iam_group_policies_list = [
    ""
  ]

 iam_group_users = [
  #""
  ]
}
```

```hcl
# Create IAM Group: CodeCommitGroup
module "iam_group_codecommit" {
  source = "git::<url>?ref=1.0.0"

  module_enabled = true

  iam_group_name        = "${var.cut_name}-CodeCommitGroup"
  iam_group_path        = "/"
  
  # iam_group_policy_name = "${var.cut_name}-CodeCommitPolicy"
  # iam_group_policy_document = <<-EOF
  # {
  #   "Version": "2012-10-17",
	#   "Statement": [
	# 		{
	# 				"Sid": "VisualEditor0",
	# 				"Effect": "Allow",
	# 				"Action": [
	# 						"codecommit:*"
	# 				],
	# 				"Resource": [
  #           "arn:aws:codecommit:us-east-1:xxxx:grlm-backend",
  #           "arn:aws:codecommit:us-east-1:xxxx:grlm-frontend"
	# 				]
	# 		}
	#   ]
  # }
  # EOF

  iam_group_policies_list = [
    #module.iam_policy_force_mfa.iam_policy_arn,
    #module.iam_policy_change_password.iam_policy_arn,
  ]

  iam_group_users = [
    #"",
  ]
}
```


## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-------:|:--------:|
| aws_default_region | The region required for AWS provider. | string | `""` | yes |
`see inputs.tf`


## Outputs

| Name | Description |
|------|-------------|
| iam_group_name | The group's name. |
`see outputs.tf`

