# ws@2023 outputs.tf

output "group_name" {
  description = "The group name."
  value       = local.create ? join("", aws_iam_group.default.*.name) : ""
}

output "group_id" {
  description = "The group ID."
  value       = local.create ? join("", aws_iam_group.default.*.id) : ""
}

output "group_arn" {
  description = "The ARN assigned by AWS for this group."
  value       = local.create ? join("", aws_iam_group.default.*.arn) : ""
}

output "group_unique_id" {
  description = "The unique ID assigned by AWS."
  value       = local.create ? join("", aws_iam_group.default.*.unique_id) : ""
}

output "group_policies" {
  description = "The list of IAM policies attached to the group."
  value       = local.create ? var.iam_group_policies_list : []
}

output "group_users" {
  description = "The list of IAM User names."
  value       = local.create ? flatten(aws_iam_group_membership.default.*.users) : []
}

