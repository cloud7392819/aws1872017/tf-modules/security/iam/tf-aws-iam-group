# ws@2023 main.tf

// See:
//   * https://www.terraform.io/docs/providers/aws/r/iam_group.html
//   * https://www.terraform.io/docs/providers/aws/r/iam_group_policy.html
//   * https://www.terraform.io/docs/providers/aws/r/iam_group_policy_attachment.html
//   * https://www.terraform.io/docs/providers/aws/r/iam_group_membership.html


##################################################################
# Manage a group resourse
##################################################################

resource "aws_iam_group" "default" {

  count = local.create ? 1 : 0

  # (Required) The group's name. The name must consist of upper and lowercase alphanumeric characters with no spaces.
  # You can also include any of the following characters: =,.@-_.. Group names are not distinguished by case.
  # For example, you cannot create groups named both "ADMINS" and "admins".
  name = var.iam_group_name

  # (Optional, default "/") Path in which to create the group.
  path = var.iam_group_path
}

##################################################################
# Attach a group policy, if a policy document is specified
##################################################################

resource "aws_iam_group_policy" "default" {

  count = (local.create && length(var.iam_group_policy_document) > 0) ? 1 : 0

  # (Optional) The name of the policy. If omitted, Terraform will assign a random, unique name.
  name    = var.iam_group_policy_name

  # (Required) The IAM group to attach to the policy.
  group   = join("", aws_iam_group.default.*.id)

  # (Required) The policy document. This is a JSON formatted string.
  policy  = var.iam_group_policy_document
}

##################################################################
# Attach IAM policies
##################################################################

# NOTE: The usage of this resource conflicts with the aws_iam_policy_attachment 
# resource and will permanently show a difference if both are defined.

resource "aws_iam_group_policy_attachment" "default" {

  count = local.create ? length(var.iam_group_policies_list) : 0

  # (Required) - The group the policy should be applied to
  group       = join("", aws_iam_group.default.*.name)

  # (Required) - The ARN of the policy you want to apply
  policy_arn  = var.iam_group_policies_list[count.index]
}


# WARNING: Multiple aws_iam_group_membership resources with the same group name will produce inconsistent behavior!
resource "aws_iam_group_membership" "default" {

# Provides a top level resource to manage IAM Group membership for IAM Users.

  # NOTE(!): `aws_iam_group_membership` will conflict with itself if used more than once with the same group.
  #          To non-exclusively manage the users in a group, see the `aws_iam_user_group_membership` resource.
  count = (local.create && length(var.iam_group_users) > 0) ? 1 : 0

  # (Required) The name to identify the Group Membership.
  name = format("%s-membership", join("", aws_iam_group.default.*.name))

  # (Required) The IAM Group name to attach the list of `users` to.
  group = join("", aws_iam_group.default.*.name)

  # (Required) A list of IAM User names to associate with the Group.
  users = flatten(data.aws_iam_user.default.*.user_name)
}

