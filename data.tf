# ws@2023 data.tf

##################################################################
# To manage group's membership the IAM user must be found first
##################################################################

data "aws_iam_user" "default" {
    
  count = local.create ? length(var.iam_group_users) : 0

  # (Required) The friendly IAM user name to match.
  user_name = var.iam_group_users[count.index]
}

