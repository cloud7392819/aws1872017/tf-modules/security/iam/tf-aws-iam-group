# ws@2023 variables.tf

##################################################################
# Common/General settings
##################################################################

variable "module_enabled" {
  description = "Whether to create the resources."
  type        = bool
  # `false` prevents the module from creating any resources
  default     = false
}

# Application name
variable "name" {
	description = "Application name"
	default		= null
}

variable "module_depends_on" {
  description = "Emulation of `depends_on` behavior for the module."
  type        = any
  # Non zero length string can be used to have current module wait for the specified resource.
  default     = ""
}

variable "module_tags" {
  description = "Additional mapping of tags to assign to the all linked resources."
  type        = map(string)
  default     = {}
}

##################################################################
# IAM GROUP 
##################################################################

variable "create" {
  description = "Whether to create the resources."
  type        = bool
  // `false` prevents the module from creating any resources
  default     = false
}

variable "iam_group_name" {
  # The name must consist of upper and lowercase alphanumeric characters with no spaces. You can also include any of
  # the following characters: `=,.@-_.`. Group names are not distinguished by case. For example, you cannot create
  # groups named both "ADMINS" and "admins".
  description = "The group's name."
  type        = string
}

variable "iam_group_path" {
  description = "Path in which to create the group."
  type        = string
  default     = "/"
}

variable "iam_group_policy_name" {
  description = "The name of the policy. If omitted, Terraform will assign a random, unique name."
  type        = string
  default     = ""
}

variable "iam_group_policy_document" {
  description = " The policy document. This is a JSON formatted string."
  type        = string
  default     = ""
}

variable "iam_group_policies_list" {
  description = "The list of IAM policies ARNs need to be attached to the IAM group."
  type        = list(string)
  default     = []
}

//variable "iam_group_policies_amount" {
//  // This `*_amount` variable might be needed to prevent Terraform error `count can't be computed`:
//  //   * https://docs.cloudposse.com/troubleshooting/terraform-value-of-count-cannot-be-computed/
//  //   * https://medium.com/@jmarhee/outputs-with-terraform-modules-ec0ce38ea1ad
//  description = "The amount of the IAM policies need to be attached."
//  default     = 0
//}

// since this resource causes exclusive ownership use only where there is no other way to configure group members.
variable "iam_group_users" {
  description = "A list of usernames to associate with the group."
  type        = list(string)
  default     = []
}


